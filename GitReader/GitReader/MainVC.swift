//
//  ViewController.swift
//  GitReader
//
//  Created by dejaWorks on 04/09/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class MainVC: UIViewController, DataAPIServiceDelegate, GitTVCDelegate {

    

    // Outlets
    @IBOutlet weak var goButtonOutlet: UIButton!
    @IBOutlet weak var keyWordTextField: UITextField!
    @IBOutlet weak var gitTableView: UITableView!
    
    
    // Actions
    @IBAction func goButton(_ sender: Any) {
        
        if let keyWord = keyWordTextField.text{
            ds.searchFor(keyWord)
        }
    }
    
    // References
    unowned private let ds = DataAPIService.shared
    
    private var gitTVC:GitTVC?
    
    private var selectedGitItem:GitItemModel?{
        didSet{
            if selectedGitItem == nil {return}
            performSegue(withIdentifier: "toDetailVC", sender: self)
        }
    }
    
    // MARK: - UNWIND Here
    @IBAction func unwindToMainVC(segue: UIStoryboardSegue) {
        print("unwindToMainVC")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ds.delegate = self
        keyWordTextField.text = ""
        connectGitTableVievController()
        title = "Git Repositories"
    }
    
    // DataAPIServiceDelegate Funcs
    internal func didDataUpdate() {
        if let gitItems = ds.gitItems, let gitTVC = gitTVC {
            print(gitItems)
            
            gitTVC.data = gitItems
        }
    }
    
    
    // GitTVCDelegate Funcs
    func didGitItemSelect(_ gitItem:GitItemModel) {
        selectedGitItem = gitItem
    }
    private func connectGitTableVievController(){
        gitTVC = GitTVC()
        gitTVC?.tableView = gitTableView
        gitTVC?.connectTheCell()
        
        addChildViewController(gitTVC!)
        gitTVC!.didMove(toParentViewController: self)
        
        gitTVC?.delegate = self
        
    }
    deinit {
        print("deinit MainVC")
    }

    
    // MARK: - NAVIGATION: Prepare for Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailVC {
            print("destination is DetailVC")
            detailVC.theGitItem = selectedGitItem
        }
    }
    
}

