//
//  ReadmeDS.swift
//  GitReader
//
//  Created by dejaWorks on 05/09/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol ReadmeDSDelegate:class {
    func didDataUpdate(_ htmlUrl:URL)
}
class ReadmeDS {

    
    weak var delegate:ReadmeDSDelegate?
    
    var htmlUrl:URL?
    
    var readmeUrlStr:String?{
        didSet{
            if readmeUrlStr == nil || readmeUrlStr == "" {return}
            
                print("requestStr: \(readmeUrlStr!)")
                
                Alamofire.request(readmeUrlStr!).responseJSON { response in
                    
                    if let json = response.result.value {
                        
                        // Create JSON object
                        let jsonObj = JSON(json)
                        
                        // readme html version url
                        let htmlUrlStr = jsonObj["html_url"].stringValue
                        
                        // Convert to URL
                        self.htmlUrl = URL(string: htmlUrlStr)
                        
                        if let url = self.htmlUrl {
                            self.delegate?.didDataUpdate(url)
                        }
                    }
                }
                
            }
    }
}
