//
//  GitItemCell.swift
//  GitReader
//
//  Created by dejaWorks on 05/09/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit

class GitItemCell: UITableViewCell {

    //IBOutlets
    @IBOutlet weak var gitNameLabel: UILabel!
    @IBOutlet weak var gitDescTextView: UITextView!
    
    
    //Cell Nib & Identifier
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    
    var data:GitItemModel?{
        didSet{
            if data == nil {return}
            
            gitNameLabel.text = data?.name
            gitDescTextView.text = data?.desc
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
