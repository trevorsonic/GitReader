//
//  GitDetailCell.swift
//  GitReader
//
//  Created by dejaWorks on 05/09/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit

class GitDetailCell: UITableViewCell {

    
    //IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    
    //Cell Nib & Identifier
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
