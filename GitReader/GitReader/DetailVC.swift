//
//  DetailVC.swift
//  GitReader
//
//  Created by dejaWorks on 05/09/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import WebKit

class DetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ReadmeDSDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var webView: WKWebView!
    
    enum DetailTypes:Int{
        case name, desc, forkQty, issueQty, readMeUrlStr
    }
    
    internal var theGitItem:GitItemModel?
    private var readmeDS:ReadmeDS?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.register(GitDetailCell.nib, forCellReuseIdentifier: GitDetailCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        
        if let theItem = theGitItem {
            title = theItem.name
            print(theItem.name)
            
            loadReadmeObj(theItem.readMeUrlStr)
        }
    }
    deinit {
        print("deinit DetailVC")
    }
    private func loadReadmeObj(_ urlStr:String){
        if readmeDS == nil {
            readmeDS = ReadmeDS()
            readmeDS?.delegate = self
            readmeDS?.readmeUrlStr = urlStr
        }
    }
    // MARK: - ReadmeDSDelegate Funcs
    func didDataUpdate(_ htmlUrl: URL) {
        print("HTML in detailsVC: \(htmlUrl.absoluteString)")
        loadReadme(htmlUrl)
    }
    private func loadReadme(_ url:URL){
        let req = URLRequest(url:url)
        self.webView!.load(req)
    }
    
    // MARK: - Table view data source
    
    // MARK: - Section qty
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: -  Row qty
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5 // This is not good hard code :-/ but just temporary
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    // MARK: - Cell Render
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GitDetailCell.identifier, for: indexPath) as! GitDetailCell
        let row  = indexPath.row
        
        // This cell render logic is not good, just temporary
        
        switch row {
        case DetailTypes.name.rawValue:
            cell.titleLabel.text = "NAME"
            cell.infoLabel.text = theGitItem?.name
            
        case DetailTypes.desc.rawValue:
            cell.titleLabel.text = "DESCRIPTION"
            cell.infoLabel.text = theGitItem?.desc
            
        case DetailTypes.forkQty.rawValue:
            cell.titleLabel.text = "NUMBER OF FORKS"
            cell.infoLabel.text = "\(theGitItem!.forksCount)"
            
        case DetailTypes.issueQty.rawValue:
            cell.titleLabel.text = "OPEN ISSUES"
            cell.infoLabel.text = "\(theGitItem!.openIssuesCount)"
        
        case DetailTypes.readMeUrlStr.rawValue:
            cell.titleLabel.text = "Readme Url"
            cell.infoLabel.text = theGitItem?.readMeUrlStr
            
        default:break;
        }
        
        return cell
    }
}
