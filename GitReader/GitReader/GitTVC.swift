//
//  GitTVC.swift
//  GitReader
//
//  Created by dejaWorks on 05/09/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit

protocol GitTVCDelegate:class {
    func didGitItemSelect(_ gitItem:GitItemModel)
}
class GitTVC: UITableViewController {

    weak var delegate:GitTVCDelegate?
    
    var data:[GitItemModel]?{
        didSet{
            tableView.reloadData()
        }
    }
    var selectedGitItem:GitItemModel?{
        didSet{
            if selectedGitItem == nil {return}
            delegate?.didGitItemSelect(selectedGitItem!)
        }
    }
    
    func connectTheCell() {
        tableView?.register(GitItemCell.nib, forCellReuseIdentifier: GitItemCell.identifier)
        tableView.delegate   = self
        tableView.dataSource = self
        
        print("ConnectCell")
        print("GitItemCell.identifier: \(GitItemCell.identifier)")
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    // MARK: - Section qty
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    // MARK: -  Row qty
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let data = data{
                return data.count
        }
        return 0
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    // MARK: - Cell Render
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GitItemCell.identifier, for: indexPath) as! GitItemCell
        if let data = data{
            let cellData = data[indexPath.row]
            cell.data = cellData
        }else{
            cell.gitNameLabel.text = "-"
            print("NIL unexpected! data[indexPath.row]")
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let data = data{
            selectedGitItem = data[indexPath.row]
        }
    }


}
