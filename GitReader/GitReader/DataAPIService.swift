//
//  DataCommunicationM.swift
//  GitReader
//
//  Created by dejaWorks on 04/09/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GitItemModel{
    public var name = ""
    public var desc = ""
    public var forksCount:Int = 0
    public var openIssuesCount:Int = 0
    public var readMeUrlStr = ""
    
    public func initWithJsonObj(_ jsonObj:JSON){
        name =              jsonObj["full_name"].stringValue
        desc =              jsonObj["description"].stringValue
        forksCount =        jsonObj["forks_count"].intValue
        openIssuesCount =   jsonObj["open_issues_count"].intValue
        
        let url =   jsonObj["url"].stringValue
        readMeUrlStr = url + "/readme"
    }
}


protocol DataAPIServiceDelegate:class {
    func didDataUpdate()
}
private let _shared = DataAPIService()
class DataAPIService {
    // MARK: - SHARED INSTANCE
    class var shared : DataAPIService {
        return _shared
    }
    
    weak var delegate:DataAPIServiceDelegate?
    
    var gitItems:[GitItemModel]?
    
    private var request: Alamofire.Request? {
        didSet {
            oldValue?.cancel()
        }
    }
    
    func searchFor(_ word:String){
        let requestStr = "https://api.github.com/search/repositories?q=\(word)"
        print("requestStr: \(requestStr)")
        
        Alamofire.request(requestStr).responseJSON { response in

            if let json = response.result.value {
               
                // Create JSON object
                let jsonObj = JSON(json)
                
                // items array: each item is a git repo
                let items = jsonObj["items"].arrayValue
                
                // create an structured array for table view
                if items.count > 0 {
                    self.createGitItemsList(items)
                }
                
            }
        }
        
    }
    private func createGitItemsList(_ items:[JSON]){
        print("items.count: \(items.count)")
        
        gitItems = [GitItemModel]()
        
        for item in items{
            let gitItem = GitItemModel()
            gitItem.initWithJsonObj(item)
            gitItems?.append(gitItem)
            //print("\(gitItem.name): \(gitItem.desc)")
            
        }
     
        // Event
        delegate?.didDataUpdate()
    }
}
