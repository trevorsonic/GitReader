//: Playground - noun: a place where people can play

import UIKit
import SwiftyJSON
import Alamofire
import PlaygroundSupport

// This is important for Alamofire async request/response
PlaygroundPage.current.needsIndefiniteExecution = true

//https://api.github.com/search/repositories?q=test

class GitItemModel{
    public var name = ""
    public var desc = ""
    public func initWithJsonObj(_ jsonObj:JSON){
        name = jsonObj["full_name"].stringValue
        desc = jsonObj["description"].stringValue
    }
}

class DataAPIService {
    
    var gitItems:[GitItemModel]?
    
    private var request: Alamofire.Request? {
        didSet {
            oldValue?.cancel()
        }
    }
    
    func searchFor(_ word:String){
        let requestStr = "https://api.github.com/search/repositories?q=\(word)"
        print("requestStr: \(requestStr)")
        
        Alamofire.request(requestStr).responseJSON { response in
            
//            print("Request: \(String(describing: response.request))")   // original url request
//            print("Response: \(String(describing: response.response))") // http url response
//            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
//                print("JSON: \(json)") // serialized json response
                
                // Create JSON object
                let jsonObj = JSON(json)
                
                // items array: each item is a git repo
                let items = jsonObj["items"].arrayValue
                
                // create an structured array for table view
                if items.count > 0 {
                    self.createGitItemsList(items)
                }
                
            }
            
            
            
//            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                print("Data: \(utf8Text)") // original server data as UTF8 string
//            }
        }
        
    }
    private func createGitItemsList(_ items:[JSON]){
        print("items.count: \(items.count)")
        for item in items{
            let gitItem = GitItemModel()
            gitItem.initWithJsonObj(item)
            
            print("\(gitItem.name): \(gitItem.desc)")
            
        }
    }
}

let ds = DataAPIService()

ds.searchFor("swifty")


